package utils;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import utils.exceptions.ConversionException;


public class Serializer<T> {
    
    private JAXBContext jaxbContext = null;
    private Unmarshaller jaxbUnmarshaller = null;
    private Marshaller jaxbMarshaller = null;
    private Gson gson = new Gson();
    
    /**
     * Constructor that creates marshaler, unmarshaler and JSON manipulation objects
     * @param packageName JAXB classes package name
     * @throws ConversionException
     */
    public Serializer(String packageName)throws ConversionException {
       try {
            jaxbContext = JAXBContext.newInstance(packageName);
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException jaxbe) {
            throw new ConversionException(jaxbe);
        }
    }
    
    private final Class<T> clazz;

    public static <U> Serializer<U> createMyGeneric(Class<U> clazz) {
        return new Serializer<U>(clazz);
    }

    protected Serializer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void doSomething() {
        T instance = clazz.newInstance();
    }

     private Class<?> extractClassFromType(Type t) throws ClassCastException {
        if (t instanceof Class<?>) {
            return (Class<?>)t;
        }
        return (Class<?>)((ParameterizedType)t).getRawType();
    }

    public Class<T> getGenericClass() {
        Class<T> result = null;
        Type type = this.getClass().getGenericSuperclass();

        if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType)type;
            Type[] fieldArgTypes = pt.getActualTypeArguments();
            result = (Class<T>)fieldArgTypes[0];
        }
        return result;
    }
    
    
    public Class returnedClass() {
         ParameterizedType parameterizedType = (ParameterizedType)getClass()
                                                     .getGenericSuperclass();
         return (Class) parameterizedType.getActualTypeArguments()[0];
    }
    
     public Type getType() {
        //TypeVariable<Class<? extends Object>>[] typevarArray = (TypeVariable<Class<? extends Object>>[])(this.getClass().getTypeParameters());
        for (TypeVariable i : this.getClass().getTypeParameters()) {
            for (Type j : i.getBounds()) {
                System.err.println(j.toString());
            }
            
            
        }
        
        Type[] typeArray = this.getClass().getTypeParameters()[0].getBounds();
        System.out.println(typeArray);
        
        Type result = null;

        return result;
    }
    

    public T readXML(String fileName) throws ConversionException {
        try {
            File file = new File(fileName);
            //JAXBContext jaxbContext = JAXBContext.newInstance(T.class);
            T value = (T)jaxbUnmarshaller.unmarshal(file);
            return value;

        } catch (JAXBException je) {
            throw new ConversionException(je);
        } catch (NumberFormatException ne) {
            throw new ConversionException(ne);    
        } catch (Exception e) {
            throw new ConversionException(e);        
        }
    }


    public void writeXML(String fileName,
                         T objectName) throws ConversionException {
        try {
            File file = new File(fileName);
            // output pretty printed
            jaxbMarshaller.marshal(objectName, file);
        } catch (JAXBException e) {
            throw new ConversionException(e);
        }
    }

    public T readJSON(String fileName, String className) throws ConversionException {
        try {
            BufferedReader file =
                new BufferedReader(new FileReader(fileName));

            char[] buffer = new char[10000];
            file.read(buffer);
            file.close();

            String json = String.valueOf(buffer).trim();
            
            //T value;
            Class classVal;
            try {
                classVal = Class.forName(className);
            } catch (ClassNotFoundException cnfe) {
                throw new ConversionException(cnfe);
            }   
            T value = (T)gson.fromJson(json, classVal);

            return value;

        } catch (IOException ioe) {
            throw new ConversionException(ioe);
        } catch (NumberFormatException ne) {
            throw new ConversionException(ne);    
        } catch (Exception e) {
            throw new ConversionException(e);        
        }
    }


    public void writeJSON(String fileName,
                          T objectName) throws ConversionException {
        try {
            PrintWriter file =
                new PrintWriter(new FileWriter(fileName));
            String json = gson.toJson(objectName);
            // output pretty printed
            file.write(json);
            file.close();

        } catch (IOException e) {
            throw new ConversionException(e);
        }
    }
    
    public InputStream serialize(T objectName) throws ConversionException {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            jaxbMarshaller.marshal(objectName, stream);
            ByteArrayInputStream outStream = new ByteArrayInputStream(stream.toByteArray());
        
            return outStream;
        } catch (JAXBException e) {
            throw new ConversionException(e);
        }
    }
    
    public T deSerialize(InputStream objectAsStream) throws ConversionException {
        try {
            objectAsStream.reset();
            T value = (T)jaxbUnmarshaller.unmarshal(objectAsStream);
            
            return value;
        } catch (JAXBException e) {
            throw new ConversionException(e);
        } catch (IOException ioe) {
            throw new ConversionException(ioe);
        }     
    }
    
    public T getJaxbFromDOM(Document doc) throws ConversionException {
        try {
            T value = (T)jaxbUnmarshaller.unmarshal(doc);
            
            return value;
        } catch (JAXBException e) {
            throw new ConversionException(e);
        }  
    }
    
    public T getJaxbFromDOMNode(Node node) throws ConversionException {
        try {
            T value = (T)jaxbUnmarshaller.unmarshal(node);
            
            return value;
        } catch (JAXBException e) {
            throw new ConversionException(e);
        }  
    }
    
    public T getJaxbFromRawXML(Source xml) throws ConversionException {
        try {
            T value = (T)jaxbUnmarshaller.unmarshal(xml);
            
            return value;
        } catch (JAXBException e) {
            throw new ConversionException(e);
        }  
    }
}
