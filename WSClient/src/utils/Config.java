package utils;

import fileio.LoadConfig;

import java.io.IOException;

import java.util.StringTokenizer;

public class Config {
    private static LoadConfig config;
    
    public static final String WSDL = getProperty("WSDL");
    public static final String SERVICE_QNAME = getProperty("SERVICE_QNAME");
    public static final String JAXB_PACKAGE = getProperty("JAXB_PACKAGE");
    public static final String PROSTORI_IN = getProperty("PROSTORI_IN");
    public static final String PROSTORI_OUT = getProperty("PROSTORI_OUT");
    public static final String PROSTORI_RES = getProperty("PROSTORI_RES");
    public static final String PROSTORI_ERR = getProperty("PROSTORI_ERR");
    public static final String RACUNI_IN = getProperty("RACUNI_IN");
    public static final String RACUNI_OUT = getProperty("RACUNI_OUT");
    public static final String RACUNI_RES = getProperty("RACUNI_RES");
    public static final String RACUNI_ERR = getProperty("RACUNI_ERR");
    public static final String PROSTORI_REF_ID = getProperty("PROSTORI_REF_ID");  
    public static final String RACUNI_REF_ID = getProperty("RACUNI_REF_ID");
    public static final int PROCESS_FREQ_MS = Integer.parseInt(getProperty("PROCESS_FREQ_MS"));
    //public static final int WS_RETRY_MS = Integer.parseInt(getProperty("WS_RETRY_MS"));
    //public static final int WS_RETRY_MAX = Integer.parseInt(getProperty("WS_RETRY_MAX"));
    public static final int CONN_RETRY_MS = Integer.parseInt(getProperty("CONN_RETRY_MS"));
    public static final int CONN_RETRY_MAX = Integer.parseInt(getProperty("CONN_RETRY_MAX"));
    public static final int CONN_TIMEOUT = Integer.parseInt(getProperty("CONN_TIMEOUT"));
    public static final int REQ_TIMEOUT = Integer.parseInt(getProperty("REQ_TIMEOUT"));
    public static final boolean IN_DELETE = Boolean.valueOf(getProperty("IN_DELETE")).booleanValue();
    public static final String PROSTOR_CLASS_NAME = "PoslovniProstorZahtjev";
    public static final String RACUN_CLASS_NAME = "RacunZahtjev";
    
    private static void loadConfiguration() throws IOException {
        if (config == null)
            config = new LoadConfig("config");
    }
    
    protected static String getProperty(String key) {
        try {
            loadConfiguration();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return config.getProperty(key);
    }
}
