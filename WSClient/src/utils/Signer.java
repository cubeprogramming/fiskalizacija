package utils;

import fis.CanonicalizationMethodType;
import fis.DigestMethodType;
import fis.KeyInfoType;
import fis.PoslovniProstorZahtjev;
import fis.RacunType;
import fis.RacunZahtjev;

import fis.ReferenceType;
import fis.SignatureMethodType;
import fis.SignatureType;

import fis.SignatureValueType;
import fis.SignedInfoType;
import fis.TransformType;
import fis.TransformsType;
import fis.X509DataType;
import fis.X509IssuerSerialType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.InputStream;

import java.io.StringReader;

import java.math.BigInteger;

import java.security.cert.CertificateException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;

import java.security.cert.X509Certificate;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;

import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.keyinfo.X509IssuerSerial;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.crypto.MarshalException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import javax.xml.transform.stream.StreamSource;

import org.apache.commons.codec.digest.DigestUtils;

import static org.junit.Assert.assertEquals;

import org.w3c.dom.Document;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import utils.exceptions.ConversionException;
import utils.exceptions.SignerException;


public class Signer {
    
    
    //Public constant that defines canonicalization method
    public static final String CANONICALIZATION_METHOD = "http://www.w3.org/2001/10/xml-exc-c14n#";
    
    /**
     * Defines Singelton design pattern
     * @return singelton Signer instance 
     */
    public static Signer getInstance() {
        if (singelton == null)
            singelton = new Signer();
        return singelton;
    }
    private static Signer singelton = null;
    
    //private String keyDbFile = null;
    
    
    
    /**
     * Izracunava i upisuje zastitni kod racuna u racunZahtjev objekat
     * @param racunZahtjev referenca na objekt tipa RacunZahtjev
     * @param keyDbFile datoeka baze sa privatnim kljucem tipa JKS
     * @param keyDbPassword lozinka JKS baze
     * @param keyPassword lozinka privatnog kljuca
     * @param dbType tip baze: Signer.JKS ili Signer.PKCS12
     * @throws SignerException wraper za sve greske povezane sa potpisivanjem
     */
    public void zastitniKodRacuna(RacunZahtjev racunZahtjev,
                                  String keyDbFile, 
                                  String keyDbPassword,
                                  String keyPassword,
                                  String dbType) throws SignerException {
        
        RacunType racun = racunZahtjev.getRacun();
        
        //Ulazni podaci za izracun
        String zaKod = racun.getOib();
        zaKod += racun.getDatVrijeme();
        zaKod += racun.getBrRac().getBrOznRac();
        zaKod += racun.getBrRac().getOznPosPr();
        zaKod += racun.getBrRac().getOznNapUr();
        //TODO Paziti da se koristi decimalna tocka a ne zarez
        zaKod += racun.getIznosUkupno().toString(); 
        
        racun.setZastKod( sign(zaKod,
                               keyDbFile, 
                               keyDbPassword,
                               keyPassword,
                               dbType) );
    }//zastitniKodRacuna
    
    /**
     * Get signed DOM Document as XML stream
     * @param <T> JAXB object type to sign
     * @param xmlSerializer reference to Serializer object
     * @param objectToSign JAXB object to be signed
     * @param referenceID root XML eelement ID attribute
     * @param keyDbFile Name and path to the JKS or PFX file
     * @param keyDbPassword Password for the key file
     * @param keyPassword Password for the private key (assumed is that only one key exist in database)
     * @param dbType Key database file type (JKS or PKCS12)
     * @return signed serialized XML
     * @throws SignerException
     */
    public <T> StreamSource getSignedXmlAsStream(Serializer<T> xmlSerializer,
                                  T objectToSign,
                                  String referenceID,
                                  String keyDbFile, 
                                  String keyDbPassword,
                                  String keyPassword,
                                  String dbType) throws SignerException {
        
            Document doc = getSignedDOM(xmlSerializer, objectToSign,referenceID, keyDbFile, 
                                        keyDbPassword, keyPassword, dbType);
            
            return getXMLStreamFromDOM(doc);
             
    }
    
    /**
     * Mathod that returns newl formed signed JAXB object of the same type as objectToSign
     * (DOES NOT WORK CORRECTLY)
     * @param <T> Type of the object that is to be signed
     * @param xmlSerializer reference to Serializer object
     * @param objectToSign JAXB object to be signed of the same type as return type
     * @param referenceID root XML eelement ID attribute
     * @param keyDbFile Name and path to the JKS or PFX file
     * @param keyDbPassword Password for the key file
     * @param keyPassword Password for the private key (assumed is that only one key exist in database)
     * @param dbType Key database file type (JKS or PKCS12)
     * @return signed JAXB object
     * @throws SignerException
     */
    public <T> T getSignedJaxbXml(Serializer<T> xmlSerializer,
                                  T objectToSign,
                                  String referenceID,
                                  String keyDbFile, 
                                  String keyDbPassword,
                                  String keyPassword,
                                  String dbType) throws SignerException {
        
        try {
            Document doc = getSignedDOM(xmlSerializer, objectToSign,referenceID, keyDbFile, 
                                        keyDbPassword, keyPassword, dbType);
            
            //DEBUG
            InputStream is = getSignedXML(doc);
            byte[] buffer = new byte[10000];
            String otput;
            try {
                int i = is.read(buffer);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            otput = new String(buffer).trim();
            System.out.println(otput);
            
            //return xmlSerializer.deSerialize(is);
            System.out.println(doc);
            //~END DEBUG
            
            return xmlSerializer.getJaxbFromDOM(doc);
            
        }  catch (ConversionException ce) {
            throw new SignerException(ce);
        }
    }
    
    public <T> void addSignToJaxbObject(
                                  T objectToSign,
                                  String referenceID,
                                  String keyDbFile, 
                                  String keyDbPassword,
                                  String keyPassword,
                                  String dbType) throws SignerException {
        

        try {
            Serializer<T> xmlSerializer = new Serializer<T>("fis");
            Document doc = getSignedDOM(xmlSerializer, objectToSign,referenceID, keyDbFile, 
                                        keyDbPassword, keyPassword, dbType);
            
            //DEBUG
            InputStream is = getSignedXML(doc);
            byte[] buffer = new byte[10000];
            String otput;
            try {
                int i = is.read(buffer);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            otput = new String(buffer).trim();
            System.out.println(otput);            
            
            //return xmlSerializer.deSerialize(is);
            System.out.println(doc);
            //~END DEBUG
            
            NodeList nodes = doc.getElementsByTagName("Signature");
            Node sign = null;
            if (nodes.getLength() >0)
                sign = nodes.item(0);
            
            //DEBUG
            TransformerFactory tf = TransformerFactory.newInstance();
            try {
                Transformer trans = tf.newTransformer(); 
                trans.transform(new DOMSource(sign), new StreamResult(System.out));
            } catch (TransformerConfigurationException tce) {
                throw new SignerException(tce);
            } catch (TransformerException te) {
                throw new SignerException(te);
            }
            //~END DEBUG
            
            SignatureType signature = getSignature(doc, referenceID, loadKey(keyDbFile,keyDbPassword,keyPassword,dbType));
            
            //Serializer<SignatureType> signatureSerializer = new Serializer<SignatureType>("fis");
            //SignatureType signature = signatureSerializer.readDOMNode(sign);
            
            if (objectToSign instanceof PoslovniProstorZahtjev) {
                ((PoslovniProstorZahtjev)objectToSign).setSignature(signature);   
            } else if (objectToSign instanceof RacunZahtjev) {
                ((RacunZahtjev)objectToSign).setSignature(signature);
            }
            
        } catch (ConversionException ce) {
            throw new SignerException(ce);
        }
    }
    
    public SignatureType getSignature(Document doc, String referenceID, PrivateKeyEntry keyEntry) {
        String digestValue, signatureVal;
        String issuerName = "OU=DEMO,O=FINA,C=HR"; 
        BigInteger issuerSerial = new BigInteger(String.valueOf(1053498955));
                              
        
        //<Signature>
        SignatureType signature = new SignatureType();
            //<SignedInfo>
            SignedInfoType signedInfo = new SignedInfoType();
                //<CanonicalizationMethod>
                CanonicalizationMethodType canonicalizationMethod = new CanonicalizationMethodType();
                canonicalizationMethod.setAlgorithm(CANONICALIZATION_METHOD);
                signedInfo.setCanonicalizationMethod(canonicalizationMethod);
        
                //<SignatureMethodT>
                SignatureMethodType signatureMethod = new SignatureMethodType();
                signatureMethod.setAlgorithm(SignatureMethod.RSA_SHA1);
                signedInfo.setSignatureMethod(signatureMethod);
                
                //<Reference>
                ReferenceType reference = new ReferenceType();
                    //<Transforms>
                    TransformsType transforms = new TransformsType();
                        //<Transform>
                        TransformType transform = new TransformType();
                        transform.setAlgorithm(Transform.ENVELOPED);
                        transforms.getTransform().add(transform);
                        //<Transform>
                        transform = new TransformType();
                        transform.setAlgorithm(CANONICALIZATION_METHOD);
                        transforms.getTransform().add(transform);
                    reference.setTransforms(transforms);
                    
                    //<DigestMethod>
                    DigestMethodType digestMethod = new DigestMethodType();
                    digestMethod.setAlgorithm(DigestMethod.SHA1);
                    reference.setDigestMethod(digestMethod);
                    reference.setDigestValue("VItfxY/A1BITZ/BuWpsGd9gKix4=".getBytes());
                reference.setURI("#"+referenceID);
                signedInfo.getReference().add(reference);
            signature.setSignedInfo(signedInfo);
            
            //<SignatureValue>
            SignatureValueType signatureValue = new SignatureValueType();
            signatureValue.setValue(("0+5UDLzJuGy56HojH510+dX6VurJmL52Ob7FDNNH8U3Nltxlo7fUvU1ra6HPSyCd4H3O\\" +
                                 "QEvZIb3xv1yqvLGYr0M5yzVGbu9o4IXG3qlTTVtEWu1MCC7OYeyltnKwr9/QRAbFrA3QWZu/F8qnc4dKg6o\\" +
                                 "hl8X8hZLLh2wIzNxtOrzbtJO6lZciCyfptR6u1vvwmUxIlo7ISCTLXw/UC9LqC86C5HBbQU+QT1i9rTsUdYZZ0\\" +
                                 "kTel4Z/ZPkYO+OqZmpiKdSbwlwwqtp7CrQ0WK2RSd+x+XUf2GF3Ydld90Tx9QYRps/Z\\" +
                                 "P0OME3jnSpKJIkRQTB8ev9rM+KYYEGbSR1rz6g==").getBytes());
            signature.setSignatureValue(signatureValue);

            //<KeyInfo>
            KeyInfoType keyInfo = new KeyInfoType();
                //<X509Data>
                X509DataType x509Data = new X509DataType();
                    //<X509Certificate>
                    //X509Certificate cert = (X509Certificate) keyEntry.getCertificate();
                    //com.rsa.certj.cert.X509Certificate x509Certificate = new com.rsa.certj.cert.X509Certificate();
                    //x509Data.getX509IssuerSerialOrX509SKIOrX509SubjectName().add(cert);
                    
                    //<X509IssuerSerial>
                    X509IssuerSerialType x509IssuerSerial = new X509IssuerSerialType();
                        //<X509IssuerName>
                        x509IssuerSerial.setX509IssuerName(issuerName);
                        //<X509SerialNumber>
                        x509IssuerSerial.setX509SerialNumber(issuerSerial);
                    x509Data.getX509IssuerSerialOrX509SKIOrX509SubjectName().add(x509IssuerSerial);
                //keyInfo.getContent().add(x509Data);
            signature.setKeyInfo(keyInfo);
        
        return signature;
    }//~getSignature
    
    private <T> Document getSignedDOM(Serializer<T> xmlSerializer,
                                  T objectToSign,
                                  String referenceID,
                                  String keyDbFile, 
                                  String keyDbPassword,
                                  String keyPassword,
                                  String dbType) throws SignerException {
        
        XMLSignatureFactory fac = getXMLSignatureFactory();
        Reference ref = getReference(fac,referenceID);
        SignedInfo si = getSignedInfo(fac, ref);
        PrivateKeyEntry keyEntry = loadKey(keyDbFile,keyDbPassword,keyPassword,dbType);
        KeyInfo ki = getKeyInfo(keyEntry, fac);
        Document doc = null;
        try {
            doc = getDocument(xmlSerializer.serialize(objectToSign));
        } catch (ConversionException ce) {
            throw new SignerException(ce);
        }
        signXML(doc, keyEntry, fac, si, ki);
        return doc;
    }
    
    /**
     * Izracunava digitalni potpis
     * @param toSign string kojeg treba potpisati
     * @param keyDbFile datoeka baze sa privatnim kljucem tipa JKS
     * @param keyDbPassword lozinka JKS baze
     * @param keyPassword lozinka privatnog kljuca
     * @param dbType tip baze: Signer.JKS ili Signer.PKCS12
     * @return String potpisani string
     * @throws SignerException wraper za sve greske povezane sa potpisivanjem
     */
    private String sign(String toSign,
                       String keyDbFile, 
                       String keyDbPassword,
                       String keyPassword,
                       String dbType) throws SignerException {
        
        //Digest izracun
        byte[] potpisano = null;
        String zastitniKod = null;
        try {
            PrivateKey privatni = loadKey(keyDbFile, keyDbPassword, keyPassword, dbType).getPrivateKey();

            //Sign
            Signature biljeznik = Signature.getInstance("SHA1withRSA");
            biljeznik.initSign(privatni);
            biljeznik.update(toSign.getBytes());
            potpisano = biljeznik.sign();
            zastitniKod = DigestUtils.md5Hex(potpisano);
                
        } catch (InvalidKeyException ike) {
            throw new SignerException(ike);
        } catch (SignatureException se) {
            throw new SignerException(se);
        } catch (NoSuchAlgorithmException nsae) {
            throw new SignerException(nsae);
        }
            
        return zastitniKod;
    }//sign
    
    /**
     * Ucitava PrivateKeyEntry iz baze (podrazumjeva se da baza ima samo jedan key)
     * @param keyDbFile datoeka baze sa privatnim kljucem tipa JKS
     * @param keyDbPassword lozinka JKS baze
     * @param keyPassword lozinka privatnog kljuca
     * @param dbType tip baze: Signer.JKS ili Signer.PKCS12
     * @return PrivateKeyEntry iz baze kljuceva
     * @throws SignerException wraper za sve greske povezane sa potpisivanjem
     */
    private PrivateKeyEntry loadKey(String keyDbFile, 
                                    String keyDbPassword,
                                    String keyPassword,
                                    String dbType) throws SignerException {
        FileInputStream fis =null;
        PrivateKeyEntry pke = null;
        KeyStore keyStore = null;
        
        try {
            //Key load
            try {
                fis = new FileInputStream(keyDbFile);
                keyStore = KeyStore.getInstance(dbType);
                keyStore.load(fis, keyDbPassword.toCharArray());
            } finally {
                if (fis != null)
                    fis.close();
            }

            pke = (PrivateKeyEntry)keyStore.getEntry(
                            keyStore.aliases().nextElement(), 
                            new KeyStore.PasswordProtection(keyPassword.toCharArray()));

        } catch (KeyStoreException kse) {
            throw new SignerException(kse);
        } catch (UnrecoverableKeyException uke) {
            throw new SignerException(uke);
        } catch (FileNotFoundException fnfe) {
            throw new SignerException(fnfe);
        } catch (UnrecoverableEntryException uee) {
            throw new SignerException(uee);
        } catch (IOException ioe) {
            throw new SignerException(ioe);
        } catch (CertificateException ce) {
            throw new SignerException(ce);
        } catch (NoSuchAlgorithmException nsae) {
            throw new SignerException(nsae);
        }
               
        return pke;               
    }//loadKey
    
    /**
     * Factory Method that returns object responsable to create <Signature> eelement
     * according to XMLDig signature specification
     * @return XMLSignatureFactory
     */
    private XMLSignatureFactory getXMLSignatureFactory() {
        return XMLSignatureFactory.getInstance("DOM");
    }
    
    /**
     * Method that returns XMLDig <Reference> element
     * containing <Transform> element with SHA1 Whole Document
     * Enveloped Transform Reference
     * @param fac XMLSignatureFactory
     * @param referenceID XML root element ID attribute
     * @return Reference XMLDig <Reference> element
     * @throws SignerException
     */
    private Reference getReference(XMLSignatureFactory fac, String referenceID) throws SignerException {   
        try {
            List transforms = new ArrayList();
            transforms.add(fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec)null));
            transforms.add(fac.newTransform(CANONICALIZATION_METHOD, (TransformParameterSpec)null));
            return fac.newReference("#" + referenceID, 
                                    fac.newDigestMethod(DigestMethod.SHA1, null), 
                                    transforms, 
                                    null,
                                    null);
        } catch (InvalidAlgorithmParameterException iape) {
            throw new SignerException(iape);
        } catch (NoSuchAlgorithmException nsae) {
            throw new SignerException(nsae);
        }
    }//getReference
    
    /**
     * Method that returns XMLDig <SignedInfo> element
     * @param fac XMLSignatureFactory
     * @param ref Reference XMLDig <Reference> element
     * @return SignedInfo XMLDig <SignedInfo> element
     * @throws SignerException
     */
    private SignedInfo getSignedInfo(XMLSignatureFactory fac, Reference ref) throws SignerException {
        try {
            return fac.newSignedInfo(fac.newCanonicalizationMethod(CANONICALIZATION_METHOD,
                                                                   (C14NMethodParameterSpec)null),
                                     fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
                                     Collections.singletonList(ref));
        } catch (InvalidAlgorithmParameterException iape) {
            throw new SignerException(iape);
        } catch (NoSuchAlgorithmException nsae) {
            throw new SignerException(nsae);
        }
    }//getSignedInfo
    
    /**
     * Method that returns XMLDig <KeyInfo> element
     * containing <X509Data> element and <X509IssuerSerial> element within it
     * @param keyEntry PrivateKeyEntry
     * @param fac XMLSignatureFactory
     * @return KeyInfo XMLDig <KeyInfo> element
     */
    private KeyInfo getKeyInfo(PrivateKeyEntry keyEntry, XMLSignatureFactory fac) {
        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();
        KeyInfoFactory kif = fac.getKeyInfoFactory();
        List x509Content = new ArrayList();
        //509Certificate
        x509Content.add(cert);
        // X509IssuerSerial
        X509IssuerSerial x509IssuerSerial = 
            kif.newX509IssuerSerial(cert.getIssuerX500Principal().getName(), cert.getSerialNumber());
        x509Content.add(x509IssuerSerial);
        
        X509Data xd = kif.newX509Data(x509Content);
        return kif.newKeyInfo(Collections.singletonList(xd));
    }//getKeyInfo
    
    /**
     * Creates DOM Document form the XML content passed as InputStream
     * @param xmlInputStream XML content passed as InputStream
     * @return Document XML parsed as DOM document
     * @throws SignerException
     */
    private Document getDocument(InputStream xmlInputStream) throws SignerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        try {
            return dbf.newDocumentBuilder().parse(xmlInputStream);
        } catch (IOException ioe) {
            throw new SignerException(ioe);
        } catch (SAXException saxe) {
            throw new SignerException(saxe);
        } catch (ParserConfigurationException pce) {
            throw new SignerException(pce);
        }
    }//getDocument
    
    /**
     * Creates DOMSignContext and signs DOM Document
     * @param doc DOM Document object to be signed
     * @param keyEntry PrivateKeyEntry
     * @param fac XMLSignatureFactory factory object for XML signatures dreation
     * @param si SignedInfo <SignedInfo> XML element
     * @param ki KeyInfo <KeyInfo> XML element
     * @throws SignerException
     */
    private void signXML(Document doc, PrivateKeyEntry keyEntry, 
                         XMLSignatureFactory fac, SignedInfo si, KeyInfo ki) throws SignerException {
        DOMSignContext dsc = new DOMSignContext(keyEntry.getPrivateKey(), doc.getDocumentElement(), doc.getDocumentElement().getLastChild());
        XMLSignature signature = fac.newXMLSignature(si, ki);
        try {
            signature.sign(dsc);
        } catch (MarshalException me) {
            throw new SignerException(me);
        } catch (XMLSignatureException xmlse) {
            throw new SignerException(xmlse);
        }
    }//signXML
    
    /**
     * Transforms DOM Document to InputStream containing signed XML
     * @param doc DOM Document that is signed
     * @return InputStream containing signed XML document
     * @throws SignerException
     */
    private InputStream getSignedXML(Document doc) throws SignerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            Transformer trans = tf.newTransformer(); 
            trans.transform(new DOMSource(doc), new StreamResult(os));
        } catch (TransformerConfigurationException tce) {
            throw new SignerException(tce);
        } catch (TransformerException te) {
            throw new SignerException(te);
        }
        ByteArrayInputStream outStream = new ByteArrayInputStream(os.toByteArray());
        
        return outStream;
    }

    /**
     * Converts DOM object to StreamSource
     * @param doc Document DOM object
     * @return StreamSource XML stream
     * @throws SignerException
     */
    private StreamSource getXMLStreamFromDOM(Document doc) throws SignerException {
        StreamSource xmlSource =
              new StreamSource(getSignedXML(doc));
        return xmlSource;
    }
}
