package utils;

import fileio.LoadConfig;

import java.io.IOException;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class KeyConfig {
    private static Map<String,LoadConfig> keyConfigs = new HashMap<String,LoadConfig>();
    
    //Public constants that thedine the type of the database
    public static final String PKCS12 = "PKCS12";
    public static final String JKS = "JKS";
    
    private static String key = "resources/fiskal1.pfx";
    private static String keyDbPassword = "614085";
    private static String keyPassword = "614085";
    
    /**
     * Ucitava properties file za svaki OIB klijenta kod svakog poziva web servisa
     * @param oib
     * @throws IOException
     */
    private static LoadConfig getConfiguration(String oib) throws IOException {
        
        if (keyConfigs.get(oib) == null)
            keyConfigs.put(oib, new LoadConfig(oib));
        
        if (keyConfigs.get(oib) == null) 
            throw new IOException("Key configuration file for OIB: " + oib + " not found");
        
        return keyConfigs.get(oib);
    }
    
    

    public static String getKey(String oib) throws IOException {
        return getConfiguration(oib).getProperty("KEY");
    }

    public static String getKeyDbPassword(String oib) throws IOException {
        return getConfiguration(oib).getProperty("KEY_DB_PASSWD");
    }

    public static String getKeyPassword(String oib) throws IOException {
        return getConfiguration(oib).getProperty("KEY_PASSWD");
    }
}
