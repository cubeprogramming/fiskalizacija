package utils.exceptions;

import java.security.SignatureException;

/**
 * Exception wrapper for all exceptions in implementing PKI signing
 */
public class SignerException extends SignatureException {
    @SuppressWarnings("compatibility:3997770590345891002")
    private static final long serialVersionUID = 877327286512597713L;

    public SignerException(Throwable throwable) {
        super(throwable);
    }

    public SignerException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public SignerException(String string) {
        super(string);
    }

    public SignerException() {
        super();
    }
}
