package utils.exceptions;

/**
 * ConversionException are reported when transforming to and from JAXB objects
 */
public class ConversionException extends Exception {
    @SuppressWarnings("compatibility:-4017788788834515379")
    private static final long serialVersionUID = -1270648823449812302L;

    public ConversionException(Throwable throwable) {
        super(throwable);
    }

    public ConversionException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public ConversionException(String string) {
        super(string);
    }

    public ConversionException() {
        super();
    }
}
