package utils.exceptions;

import com.sun.tools.ws.processor.ProcessorException;

/**
 * ProcessorException are reported for all kind of problems in web service invocation
 */
public class ProcessException extends ProcessorException {
    @SuppressWarnings("compatibility:6601379335430682359")
    private static final long serialVersionUID = 1L;

    public ProcessException(Throwable throwable) {
        super(throwable);
    }

    public ProcessException(String string) {
        super(string);
    }

    public ProcessException(String string, Object[] objects) {
        super(string, objects);
    }
}
