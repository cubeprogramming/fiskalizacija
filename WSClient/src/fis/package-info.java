//@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.w3.org/2000/09/xmldsig#", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
@XmlSchema(
    namespace = "http://www.apis-it.hr/fin/2012/types/f73",
    elementFormDefault = XmlNsForm.QUALIFIED,
    xmlns={
        @XmlNs(prefix="tns", 
               namespaceURI="http://www.apis-it.hr/fin/2012/types/f73"),
        @XmlNs(prefix="", 
               namespaceURI="http://www.w3.org/2000/09/xmldsig#")}) 
package fis;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
