package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { RacunZahtjevTest.class, 
                       FiskalizacijaPortTypeTest.class,
                       FiskalizacijaPortTypeClientTest.class})
public class AllTests {
}
