package test;

import fis.PoslovniProstorOdgovor;
import fis.PoslovniProstorZahtjev;
import fis.RacunOdgovor;
import fis.RacunZahtjev;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.UUID;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import uis.FiskalizacijaPortTypeClient;

import utils.Config;
import utils.KeyConfig;

import utils.exceptions.ConversionException;
import utils.Serializer;
import utils.Signer;
import utils.exceptions.SignerException;

public class FiskalizacijaPortTypeClientTest {
    FiskalizacijaPortTypeClient fiskalizacijaClient;
    Serializer<RacunZahtjev> racunSer = null;
    Serializer<PoslovniProstorZahtjev> prostorSer = null;
    
    public FiskalizacijaPortTypeClientTest() {
        try {
            racunSer = new Serializer<RacunZahtjev>("fis");
            prostorSer = new Serializer<PoslovniProstorZahtjev>("fis");
        } catch (ConversionException ce) {
            throw new AssertionError(ce.getMessage());
        } 
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        fiskalizacijaClient = new FiskalizacijaPortTypeClient();
    }

    @After
    public void tearDown() throws Exception {
        fiskalizacijaClient = null;
    }

    /**
     * @see uis.FiskalizacijaPortTypeClient#racuni(fis.RacunZahtjev)
     */
    @Test
    public void testRacuni() {
        fail("Unimplemented");
    }

    /**
     * @see uis.FiskalizacijaPortTypeClient#poslovniProstor(fis.PoslovniProstorZahtjev)
     */
    @Test
    public void testPoslovniProstor() {
        fail("Unimplemented");
    }

    /**
     * @see uis.FiskalizacijaPortTypeClient#echo(String)
     */
    @Test
    public void testEcho() {
        assertEquals("Not valid response", "echoTest", fiskalizacijaClient.echo("echoTest"));
    }
    
    /**
     * @see uis.FiskalizacijaPortTypeClient#getRawXmlResponse(javax.xml.transform.stream.StreamSource)
     */
    @Test
    public void testGetRawXmlResponse() {
        Signer signer = Signer.getInstance();
        
        RacunZahtjev racunZahtjev;
        PoslovniProstorZahtjev poslovniProstorZahtjev;

        //Read Racun
        try {
            racunZahtjev = racunSer.readJSON("resources/serialized/racunZahtjev.json","fis.RacunZahtjev");
            poslovniProstorZahtjev = prostorSer.readJSON("resources/serialized/poslovniProstorZahtjev.json","fis.PoslovniProstorZahtjev");
            //racunZahtjev = racunSer.readXML("resources/serialized/racunZahtjev.xml");
            //poslovniProstorZahtjev = prostorSer.readXML("resources/serialized/poslovniProstorZahtjev.xml");

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
        
        assertNotNull("Null file", racunZahtjev);
        
        //Definiranje UUID-a
        racunZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());
        poslovniProstorZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());

        //Definiraj datum
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            racunZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
            poslovniProstorZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new AssertionError(e.getMessage());
        }
        
        //Upisi zastitni kod
        try {
            signer.zastitniKodRacuna(racunZahtjev,
                                     "resources/fiskal1.pfx",
                                     "614085", 
                                     "614085",
                                     KeyConfig.PKCS12);
        } catch (SignerException se) {
            se.printStackTrace();
            throw new AssertionError(se.getMessage());
        }
        
        //Test XML Signing
        StreamSource xmlSourceProstor = null;
        StreamSource xmlSourceRacun = null;
        try {
            xmlSourceProstor = signer.getSignedXmlAsStream(prostorSer, 
                                                   poslovniProstorZahtjev, 
                                                   "poslovniProstorId", 
                                                   "resources/fiskal1.pfx", 
                                                   "614085",
                                                   "614085", 
                                                   KeyConfig.PKCS12);
             xmlSourceRacun = signer.getSignedXmlAsStream(racunSer, 
                                                   racunZahtjev, 
                                                   "racunId", 
                                                   "resources/fiskal1.pfx", 
                                                   "614085",
                                                   "614085", 
                                                   KeyConfig.PKCS12);
            
        } catch (SignerException se) {
            se.printStackTrace();
            throw new AssertionError(se.getMessage());
        }
        
        Source prostorRawXML = fiskalizacijaClient.getRawXmlResponse(xmlSourceProstor);
        Source racunRawXML = fiskalizacijaClient.getRawXmlResponse(xmlSourceRacun);
        
        assertNotNull("Response not received for prostor", prostorRawXML);
        //assertNotNull("Response not received for racun", racunRawXML);
        
        PoslovniProstorOdgovor prostorResp;
        RacunOdgovor racunResp;
        Serializer<PoslovniProstorOdgovor> prostorRespSer;
        Serializer<RacunOdgovor> racunRespSer;
        try {
            prostorRespSer = new Serializer<PoslovniProstorOdgovor>("fis");
            prostorResp = prostorRespSer.getJaxbFromRawXML(prostorRawXML);
            
            racunRespSer = new Serializer<RacunOdgovor>("fis");
            racunResp = racunRespSer.getJaxbFromRawXML(racunRawXML);
        } catch (ConversionException e) {
            throw new AssertionError(e.getMessage());
        }
        
        //Write response
        try {

            prostorRespSer.writeXML("resources/serialized/poslovniProstorResonse.xml",prostorResp);
            racunRespSer.writeXML("resources/serialized/racunResponse.xml",racunResp);

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
        
    }
}
