package test;

import fis.PoslovniProstorZahtjev;
import fis.RacunZahtjev;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import org.junit.Test;

import utils.Config;
import utils.KeyConfig;

import utils.exceptions.ConversionException;
import utils.Serializer;
import utils.Signer;
import utils.exceptions.SignerException;

public class SerializerTest {
    Serializer<RacunZahtjev> racunSer = null;
    Serializer<PoslovniProstorZahtjev> prostorSer = null;
    
    public SerializerTest() {
        try {
            racunSer = new Serializer<RacunZahtjev>("fis");
            prostorSer = new Serializer<PoslovniProstorZahtjev>("fis");
        } catch (ConversionException ce) {
            throw new AssertionError(ce.getMessage());
        }  
    }

    /**
     * @see Serializer#readXML(String)
     */
    @Test
    public void testReadWriteXML() {
    
        RacunZahtjev racunZahtjev;
        PoslovniProstorZahtjev poslovniProstorZahtjev;

        //Read Racun
        try {

            racunZahtjev = racunSer.readXML("resources/serialized/racunZahtjev");
            poslovniProstorZahtjev = prostorSer.readXML("resources/serialized/poslovniProstorZahtjev");

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
        
        assertNotNull("Null file", racunZahtjev);
        
        //Definiraj ID
        racunZahtjev.setId("racunId");
        poslovniProstorZahtjev.setId("poslovniProstorId");
            
        //Definiranje UUID-a
        racunZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());
        poslovniProstorZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());

        //Definiraj datum
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            racunZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
            poslovniProstorZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new AssertionError(e.getMessage());
        }
        
        //Upisi zastitni kod
        try {
            Signer.getInstance().zastitniKodRacuna(racunZahtjev,
                                                   "resources/fina.jks",
                                                   "614085", 
                                                   "614085",
                                                   KeyConfig.JKS);
        } catch (SignerException se) {
            se.printStackTrace();
            throw new AssertionError(se.getMessage());
        };

   

        //Write racun
        try {
            racunSer.writeXML("resources/serialized/racunZahtjev_out",racunZahtjev);
            prostorSer.writeXML("resources/serialized/poslovniProstorZahtjev_out",poslovniProstorZahtjev);

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }  
    }

    /**
     * @see Serializer#readJSON(String,String)
     */
    @Test
    public void testReadWriteJSON() {
        RacunZahtjev racunZahtjev;
        PoslovniProstorZahtjev poslovniProstorZahtjev;

        //Read Racun
        try {

            racunZahtjev = racunSer.readJSON("resources/serialized/racunZahtjev.json","fis.RacunZahtjev");
            poslovniProstorZahtjev = prostorSer.readJSON("resources/serialized/poslovniProstorZahtjev.json","fis.PoslovniProstorZahtjev");
            //racunZahtjev = racunSer.readXML("resources/serialized/racunZahtjev.xml");
            //poslovniProstorZahtjev = prostorSer.readXML("resources/serialized/poslovniProstorZahtjev.xml");
        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
        
        assertNotNull("Null file", racunZahtjev);

        //Definiraj ID
        racunZahtjev.setId("racunId");
        poslovniProstorZahtjev.setId("poslovniProstorId");

        //Definiranje UUID-a
        racunZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());
        poslovniProstorZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());

        //Definiraj datum
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            racunZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
            poslovniProstorZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new AssertionError(e.getMessage());
        }
        
        //Upisi zastitni kod
        try {
            Signer.getInstance().zastitniKodRacuna(racunZahtjev,
                                                   "resources/fina.jks",
                                                   "614085", 
                                                   "614085",
                                                   KeyConfig.JKS);
        } catch (SignerException se) {
            se.printStackTrace();
            throw new AssertionError(se.getMessage());
        };

        //Write racun
        try {

            racunSer.writeJSON("resources/serialized/racunZahtjev_out.json",racunZahtjev);
            prostorSer.writeJSON("resources/serialized/poslovniProstorZahtjev_out.json",poslovniProstorZahtjev);

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }  
    }
    
    /**
     * @see Serializer#readXML(String)
     */
    @Test
    public void testReadJSONWriteXML() {
        
        
        RacunZahtjev racunZahtjev;
        PoslovniProstorZahtjev poslovniProstorZahtjev;

        //Read Racun
        try {

            racunZahtjev = racunSer.readJSON("resources/serialized/racunZahtjev.json","fis.RacunZahtjev");
            poslovniProstorZahtjev = prostorSer.readJSON("resources/serialized/poslovniProstorZahtjev.josn","fis.PoslovniProstorZahtjev");

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
        
        assertNotNull("Null file", racunZahtjev);

        //Definiraj ID
        racunZahtjev.setId("racunId");
        poslovniProstorZahtjev.setId("poslovniProstorId");

        //Definiranje UUID-a
        racunZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());
        poslovniProstorZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());

        //Definiraj datum
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            racunZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
            poslovniProstorZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));racunZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new AssertionError(e.getMessage());
        }
        
        //Upisi zastitni kod
        try {
            Signer.getInstance().zastitniKodRacuna(racunZahtjev,
                                                   "resources/fiskal1.pfx",
                                                   "614085", 
                                                   "614085",
                                                   KeyConfig.PKCS12);
        } catch (SignerException se) {
            se.printStackTrace();
            throw new AssertionError(se.getMessage());
        };

        //Write racun
        try {

            racunSer.writeXML("resources/serialized/racunZahtjev_json.xml",racunZahtjev);
            prostorSer.writeXML("resources/serialized/poslovniProstorZahtjev_json.xml",poslovniProstorZahtjev);

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }  
    }
    
    /**
     * @see Serializer#readXML(String)
     */
    @Test
    public void testSerializeDeserialize() {
        
        
        RacunZahtjev racunZahtjev;
        PoslovniProstorZahtjev poslovniProstorZahtjev;

        //Read Racun
        try {

            racunZahtjev = racunSer.readXML("resources/serialized/racunZahtjev.xml");
            poslovniProstorZahtjev = prostorSer.readXML("resources/serialized/poslovniProstorZahtjev.xml");

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
        
        assertNotNull("Null file", racunZahtjev);
        assertNotNull("Null file", poslovniProstorZahtjev);
        
        //Definiraj ID
        racunZahtjev.setId("racunId");
        poslovniProstorZahtjev.setId("poslovniProstorId");
            
        //Definiranje UUID-a
        racunZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());
        poslovniProstorZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());

        //Definiraj datum
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            racunZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
            poslovniProstorZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new AssertionError(e.getMessage());
        }
        
        //Upisi zastitni kod
        try {
            Signer.getInstance().zastitniKodRacuna(racunZahtjev,
                                                   "resources/fina.jks",
                                                   "614085", 
                                                   "614085",
                                                   KeyConfig.JKS);
        } catch (SignerException se) {
            se.printStackTrace();
            throw new AssertionError(se.getMessage());
        };

    

        //test Serialization
        InputStream stream =null;
        byte[] buffer = new byte[10000];
        String otput;
        try { 
            stream = racunSer.serialize(racunZahtjev);
            int i = stream.read(buffer);
            otput = new String(buffer).trim();
            System.out.println(otput);
            
        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        } catch (IOException ioe) {
            ioe.printStackTrace();
            throw new AssertionError(ioe.getMessage());
        } 
        
        //test DeSerialization
        try {
            //ByteArrayInputStream instr = new ByteArrayInputStream(otput.getBytes());
            racunZahtjev  = racunSer.deSerialize(stream);
            racunSer.writeXML("resources/serialized/racunZahtjev_deser.xml",racunZahtjev);
            
        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
    }
    
    /**
     * @see Serializer#readXML(String)
     */
    @Test
    public void testReadJSONWriteSignedXML() {
        Signer signer = Signer.getInstance();
        
        RacunZahtjev racunZahtjev;
        PoslovniProstorZahtjev poslovniProstorZahtjev;

        //Read Racun
        try {
            racunZahtjev = racunSer.readJSON("resources/serialized/racunZahtjev.json","fis.RacunZahtjev");
            poslovniProstorZahtjev = prostorSer.readJSON("resources/serialized/poslovniProstorZahtjev.json","fis.PoslovniProstorZahtjev");
            //racunZahtjev = racunSer.readXML("resources/serialized/racunZahtjev.xml");
            //poslovniProstorZahtjev = prostorSer.readXML("resources/serialized/poslovniProstorZahtjev.xml");

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
        
        assertNotNull("Null file", racunZahtjev);

        //Definiraj ID
        //racunZahtjev.setId("racunId");
        //poslovniProstorZahtjev.setId("poslovniProstorId");
        //Check Id
        String prostoriId = poslovniProstorZahtjev.getId();
        System.out.println(prostoriId);
        String racunId = racunZahtjev.getId();
        System.out.println(racunId);
        
        
        //Definiranje UUID-a
        racunZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());
        poslovniProstorZahtjev.getZaglavlje().setIdPoruke(UUID.randomUUID().toString());

        //Definiraj datum
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            racunZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
            poslovniProstorZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));racunZahtjev.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new AssertionError(e.getMessage());
        }
        
        //Upisi zastitni kod
        try {
            signer.zastitniKodRacuna(racunZahtjev,
                                     "resources/fiskal1.pfx",
                                     "614085", 
                                     "614085",
                                     KeyConfig.PKCS12);
        } catch (SignerException se) {
            se.printStackTrace();
            throw new AssertionError(se.getMessage());
        };

        //Test XML Signing
        try {
            /* poslovniProstorZahtjev = signer.getSignedJaxbXml(prostorSer, 
                                                   poslovniProstorZahtjev, 
                                                   "poslovniProstorId", 
                                                   "resources/fiskal1.pfx", 
                                                   "614085",
                                                   "614085", 
                                                   Signer.PKCS12); */
            /* racunZahtjev = signer.getSignedJaxbXml(racunSer, 
                                                   racunZahtjev, 
                                                   "racunId", 
                                                   "resources/fiskal1.pfx", 
                                                   "614085",
                                                   "614085", 
                                                   Signer.PKCS12); */
            signer.addSignToJaxbObject(racunZahtjev, 
                                       "racunId", 
                                       "resources/fiskal1.pfx", 
                                       "614085",
                                       "614085", 
                                       KeyConfig.PKCS12);
            
        } catch (SignerException se) {
            se.printStackTrace();
            throw new AssertionError(se.getMessage());
        } 

        //Test signature
        String signature = racunZahtjev.getSignature().getSignedInfo().getCanonicalizationMethod().getAlgorithm();
        System.out.println(signature);
        
        //Write racun
        try {

            racunSer.writeXML("resources/serialized/racunZahtjev_signed.xml",racunZahtjev);
            prostorSer.writeXML("resources/serialized/poslovniProstorZahtjev_signed.xml",poslovniProstorZahtjev);

        } catch (ConversionException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }  
    }
}
