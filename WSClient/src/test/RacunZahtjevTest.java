package test;

import fis.BrojRacunaType;
import fis.NacinPlacanjaType;
import fis.NaknadaType;
import fis.NaknadeType;
import fis.OstaliPoreziType;
import fis.OznakaSlijednostiType;
import fis.PdvType;
import fis.PorezNaPotrosnjuType;
import fis.PorezOstaloType;
import fis.PorezType;
import fis.RacunType;
import fis.RacunZahtjev;

import fis.ZaglavljeType;

import java.io.FileOutputStream;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;

import java.math.BigDecimal;

import com.google.gson.Gson;

//import com.rsa.certj.cert.X509Certificate;

import fis.CanonicalizationMethodType;
import fis.DigestMethodType;
import fis.KeyInfoType;
import fis.ObjectType;
import fis.ReferenceType;
import fis.SignatureMethodType;
import fis.SignatureType;

import fis.SignatureValueType;
import fis.SignedInfoType;

import fis.TransformType;
import fis.TransformsType;

import fis.X509DataType;
import fis.X509IssuerSerialType;

import java.io.File;

import java.security.cert.X509Certificate;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.UUID;

//import javax.security.cert.X509Certificate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class RacunZahtjevTest {
    private RacunZahtjev racunZahtjev;
    private static int incr = 0;
    private FileOutputStream fileOutputStream;
    private ObjectOutputStream objectOutputStream;

    public RacunZahtjevTest() {
        racunZahtjev = new RacunZahtjev();
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    /**
     * @see fis.RacunZahtjev#setZaglavlje(fis.ZaglavljeType)
     */
    @Test
    public void testSetZaglavlje() {


        ZaglavljeType zaglavlje = new ZaglavljeType();


        //Definiranje UUID-a
        UUID idPoruke = UUID.randomUUID();
        zaglavlje.setIdPoruke(idPoruke.toString());

        //zaglavlje.setDatumVrijeme("01.09.2012T21:10:34");
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            zaglavlje.setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new AssertionError(e.getMessage());
        }


        racunZahtjev.setZaglavlje(zaglavlje);
        //System.err.println(zaglavlje);
        try {
            incr++;
            fileOutputStream =
                    new FileOutputStream("resources/serialized/object" + incr +
                                         ".ser");
            objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(zaglavlje);
        } catch (IOException ioerr) {
            //System.err.println(ioerr.getMessage());
            ioerr.printStackTrace();
            throw new AssertionError(ioerr.getMessage());
        }


        assertEquals("Not matching", zaglavlje, racunZahtjev.getZaglavlje());
    }


    /**
     * @see fis.RacunZahtjev#setRacun(fis.RacunType)
     */
    @Test
    public void testSetRacun() {
        RacunType racun = new RacunType();

        racun.setOib("98765432198");
        racun.setUSustPdv(true);
        racun.setDatVrijeme("01.09.2012T21:10:34");
        racun.setOznSlijed(OznakaSlijednostiType.P);

        BrojRacunaType brojRacuna = new BrojRacunaType();
        brojRacuna.setBrOznRac("123456789");
        brojRacuna.setOznPosPr("POSL1");
        brojRacuna.setOznNapUr("12");
        racun.setBrRac(brojRacuna);

        PdvType pdvType = new PdvType();
        PorezType porezType = new PorezType();
        porezType.setStopa(new BigDecimal(25.00));
        porezType.setOsnovica(new BigDecimal(10.00));
        porezType.setIznos(new BigDecimal(2.50));
        pdvType.getPorez().add(porezType);
        porezType = new PorezType();
        porezType.setStopa(new BigDecimal(10.00));
        porezType.setOsnovica(new BigDecimal(10.00));
        porezType.setIznos(new BigDecimal(1.00));
        pdvType.getPorez().add(porezType);
        porezType = new PorezType();
        porezType.setStopa(new BigDecimal(0.00));
        porezType.setOsnovica(new BigDecimal(10.00));
        porezType.setIznos(new BigDecimal(0.50));
        pdvType.getPorez().add(porezType);
        racun.setPdv(pdvType);

        PorezNaPotrosnjuType porezNaPotrosnjuType = new PorezNaPotrosnjuType();
        porezType = new PorezType();
        porezType.setStopa(new BigDecimal(3.00));
        porezType.setOsnovica(new BigDecimal(10.00));
        porezType.setIznos(new BigDecimal(0.30));
        porezNaPotrosnjuType.getPorez().add(porezType);
        racun.setPnp(porezNaPotrosnjuType);

        OstaliPoreziType ostaliPoreziType = new OstaliPoreziType();
        PorezOstaloType porezOstaloType = new PorezOstaloType();
        porezOstaloType.setNaziv("Porez na luksuz");
        porezOstaloType.setStopa(new BigDecimal(15.00));
        porezOstaloType.setOsnovica(new BigDecimal(10.00));
        porezOstaloType.setIznos(new BigDecimal(1.50));
        ostaliPoreziType.getPorez().add(porezOstaloType);
        racun.setOstaliPor(ostaliPoreziType);

        racun.setIznosOslobPdv(new BigDecimal(12.00));
        racun.setIznosMarza(new BigDecimal(13.00));
        racun.setIznosNePodlOpor(new BigDecimal(100.00));

        NaknadeType naknadeType = new NaknadeType();
        NaknadaType naknadaType = new NaknadaType();
        naknadaType.setNazivN("Povratna naknada");
        naknadaType.setIznosN(new BigDecimal(1.00));
        naknadeType.getNaknada().add(naknadaType);
        racun.setNaknade(naknadeType);

        racun.setIznosUkupno(new BigDecimal(145.68));
        racun.setNacinPlac(NacinPlacanjaType.K);
        racun.setOibOper("01234567890");
        racun.setZastKod("e4d909c290d0fb1ca068ffaddf22cbd0");
        racun.setNakDost(false);
        racun.setParagonBrRac("123/458/5");
        racun.setSpecNamj("Navedeno kao primjer");

        racunZahtjev.setRacun(racun);
        //System.err.println(racun);
        try {
            PrintWriter file =
                new PrintWriter(new FileWriter("resources/serialized/racunZahtjev2.json"));

            Gson gson = new Gson();
            String json = gson.toJson(racunZahtjev);

            // output pretty printed
            file.write(json);

            file.close();
        } catch (IOException ioerr) {
            ioerr.printStackTrace();
            throw new AssertionError(ioerr.getMessage());
        }

        assertEquals("Not matching", racun, racunZahtjev.getRacun());
    }


    /**
     * @see fis.RacunZahtjev#setSignature(fis.SignatureType)
     */
    @Test
    public void testSetSignature() {
        SignatureType signature = new SignatureType();

        SignedInfoType signedInfo = new SignedInfoType();
        CanonicalizationMethodType canonicalizationMethod =
            new CanonicalizationMethodType();
        //canonicalizationMethod.getContent().add("http://www.w3.org/2001/10/xml-exc-c14n#");
        canonicalizationMethod.setAlgorithm("http://www.w3.org/2001/10/xml-exc-c14n#");
        signedInfo.setCanonicalizationMethod(canonicalizationMethod);
        SignatureMethodType signatureMethod = new SignatureMethodType();
        //signatureMethod.getContent().add(arg0);
        signatureMethod.setAlgorithm("http://www.w3.org/2000/09/xmldsig#rsa-sha1");
        signedInfo.setSignatureMethod(signatureMethod);
        ReferenceType reference = new ReferenceType();
        TransformsType transforms = new TransformsType();
        TransformType transform = new TransformType();
        //transform.getContent().add(arg0);
        transform.setAlgorithm("http://www.w3.org/2000/09/xmldsig#enveloped-signature");
        transforms.getTransform().add(transform);
        transform = new TransformType();
        transform.setAlgorithm("http://www.w3.org/2001/10/xml-exc-c14n#");
        transforms.getTransform().add(transform);
        reference.setTransforms(transforms);
        DigestMethodType digestMethod = new DigestMethodType();
        //digestMethod.getContent().add(arg0);
        digestMethod.setAlgorithm("http://www.w3.org/2000/09/xmldsig#sha1");
        reference.setDigestMethod(digestMethod);
        reference.setDigestValue("VItfxY/A1BITZ/BuWpsGd9gKix4=".getBytes());
        //reference.setId("");
        //reference.setURI("");
        //reference.setType("");
        signedInfo.getReference().add(reference);
        signature.setSignedInfo(signedInfo);


        SignatureValueType signatureValue = new SignatureValueType();
        signatureValue.setValue(("0+5UDLzJuGy56HojH510+dX6VurJmL52Ob7FDNNH8U3Nltxlo7fUvU1ra6HPSyCd4H3O\\" +
                                 "QEvZIb3xv1yqvLGYr0M5yzVGbu9o4IXG3qlTTVtEWu1MCC7OYeyltnKwr9/QRAbFrA3QWZu/F8qnc4dKg6o\\" +
                                 "hl8X8hZLLh2wIzNxtOrzbtJO6lZciCyfptR6u1vvwmUxIlo7ISCTLXw/UC9LqC86C5HBbQU+QT1i9rTsUdYZZ0\\" +
                                 "kTel4Z/ZPkYO+OqZmpiKdSbwlwwqtp7CrQ0WK2RSd+x+XUf2GF3Ydld90Tx9QYRps/Z\\" +
                                 "P0OME3jnSpKJIkRQTB8ev9rM+KYYEGbSR1rz6g==").getBytes());
        //signatureValue.setId("");

        KeyInfoType keyInfo = new KeyInfoType();
        X509DataType x509Data = new X509DataType();
        //X509Certificate x509Certificate = new X509Certificate();
        //X509IssuerSerialType x509IssuerSerial = new X509IssuerSerialType();
        //x509Data.getX509IssuerSerialOrX509SKIOrX509SubjectName().add(x509Certificate);
        //x509Data.getX509IssuerSerialOrX509SKIOrX509SubjectName().add(x509IssuerSerial);
        //keyInfo.getContent().add(x509Data);
        //keyInfo.setId("");

        // List<ObjectType> object = new List<ObjectType>();

        signature.setId("");
        
        racunZahtjev.setSignature(signature);

        assertEquals("Not matching", signature, racunZahtjev.getSignature());
    }

    /**
     * @see fis.RacunZahtjev#setZaglavlje(fis.ZaglavljeType)
     */
    @Test
    public void testReadWriteRacun_XML() {
        RacunZahtjev racunZahtjev_in;
        JAXBContext jaxbContext;

        //Read Racun
        try {

            File file_in = new File("resources/serialized/racunZahtjev.xml");
            jaxbContext = JAXBContext.newInstance(RacunZahtjev.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            racunZahtjev_in =
                    (RacunZahtjev)jaxbUnmarshaller.unmarshal(file_in);

        } catch (JAXBException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }
        
        assertNotNull("Null file", racunZahtjev_in);

        //Definiranje UUID-a
        UUID idPoruke = UUID.randomUUID();
        racunZahtjev_in.getZaglavlje().setIdPoruke(idPoruke.toString());

        //Definiraj datum
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            racunZahtjev_in.getZaglavlje().setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new AssertionError(e.getMessage());
        }

        //Write racun
        try {

            File file_out = new File("resources/serialized/racunZahtjev_sign.xml");
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(racunZahtjev_in, file_out);

        } catch (JAXBException e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }


        assertEquals("Not matching", racunZahtjev_in, racunZahtjev_in);
    }
}
