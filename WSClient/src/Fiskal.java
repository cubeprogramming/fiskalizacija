import fileio.FilesUtil;

import fis.PoslovniProstorOdgovor;
import fis.PoslovniProstorZahtjev;
import fis.RacunOdgovor;
import fis.RacunZahtjev;

import fis.ZaglavljeType;

import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import uis.FiskalizacijaPortTypeClient;

import uis.FiskalizacijaService;

import utils.Config;

import utils.exceptions.ProcessException;

import utils.KeyConfig;

import utils.exceptions.ConversionException;
import utils.Serializer;
import utils.Signer;
import utils.exceptions.SignerException;

public class Fiskal {
    public static void main(String[] args) throws ProcessException {
        new Fiskal();
    }
    
    private final static Logger logger = Logger.getLogger(FiskalizacijaService.class.getName());
       
    private FiskalizacijaPortTypeClient fiskalizacijaClient = new FiskalizacijaPortTypeClient();   
        
    private FilesUtil files = new FilesUtil();
    Serializer<RacunZahtjev> racunSer = null;
    Serializer<PoslovniProstorZahtjev> prostorSer = null;
    Serializer<RacunOdgovor> racunRespSer = null;
    Serializer<PoslovniProstorOdgovor> prostorRespSer = null;
    
    public Fiskal() throws ProcessException {
        try {
            System.out.println("#### Initializing FiskalizacijaService-Client ####");
            racunSer = new Serializer<RacunZahtjev>(Config.JAXB_PACKAGE);
            prostorSer = new Serializer<PoslovniProstorZahtjev>(Config.JAXB_PACKAGE);
            racunRespSer = new Serializer<RacunOdgovor>(Config.JAXB_PACKAGE);
            prostorRespSer = new Serializer<PoslovniProstorOdgovor>(Config.JAXB_PACKAGE);
        } catch (ConversionException ce) {
            logger.logp(Level.SEVERE, Fiskal.class.getName(), "Fiskal()",
                        "Error in executin Web Service client with message: "+ce.getMessage());
            throw new ProcessException(ce);
        }
        
        runAsDaemon();
   
    }//Fiskal()
    
    private void runAsDaemon() throws ProcessException {
        while (true) {
            //Execute Prostori processing
            System.out.println(" ");
            System.out.println(" ");
            System.out.println("Processign PoslovniProstor input directory");

            processFiles(Config.PROSTORI_IN, Config.PROSTORI_OUT,
                         Config.PROSTORI_ERR, Config.PROSTORI_RES,
                         prostorSer, prostorRespSer,
                         Config.PROSTORI_REF_ID,
                         Config.PROSTOR_CLASS_NAME);

            //Execute Racuni processing
            System.out.println("Processing racuni input directory");

            processFiles(Config.RACUNI_IN, Config.RACUNI_OUT,
                         Config.RACUNI_ERR, Config.RACUNI_RES, racunSer,
                         racunRespSer, Config.RACUNI_REF_ID,
                         Config.RACUN_CLASS_NAME);

            //Wait for the next cycle
            try {
                Thread.sleep(Config.PROCESS_FREQ_MS);
            } catch (InterruptedException e) {
                e.printStackTrace();
                logger.logp(Level.SEVERE, Fiskal.class.getName(), "runAsDaemon()",
                            "Execution of web service interrupted with message: "+e.getMessage());
                throw new ProcessException("Execution of web service interrupted");   
            }
        }
    }
    
    /**
     * Generic method to process input files, call web service and write results
     * @param <T> Type of input JAXB object to web service
     * @param inputDir Directory that contains input files
     * @param outDir Directoriy with sucessfuly procesed files
     * @param errDir Directory with unrecognizable files
     * @param resDir Directory containing responses
     * @param ser Serializer for input JAXB objects
     * @param respSer Serializer for output JAXB objects
     * @param refId Reference ID for the root XML element
     * @param className JSON processing requires class name for transformer so
     *        we have to pass 'ProstoriZahtjev' or 'RacuniZahtjev'
     * @throws ProcessException specific web service processing exception
     */
    private <T,V> void processFiles(String inputDir, 
                                  String outDir, 
                                  String errDir,
                                  String resDir,
                                  Serializer<T> ser,
                                  Serializer<V> respSer,  
                                  String refId,
                                  String className) throws ProcessException {
 
        Signer signer = Signer.getInstance();
        Set<Map.Entry<Boolean, String>> fileList;

        try {
            System.out.println("Loading list of files to process");
            System.out.println(" ");
            fileList = files.getFileLst(inputDir).entrySet();
            if (fileList.size() == 0 )
                System.out.println("Nothing to process. Input Directory is empty!");
            
        } catch (IOException e) {
            throw new ProcessException(e);
        }
        
        //Process every file found in the list
        for (Map.Entry<Boolean, String> file :fileList) {
            T jaxbObject;
            
            //Read from in
            try {
                jaxbObject =readFile(ser, inputDir,file, className);
                System.out.println("File "+ file.getValue() +" converted to JAXB object");
                
            } catch (ProcessException pe) {
                //Copy file that can not be initialy processed to errors
                try {
                    files.copyFile(inputDir, file.getValue(), errDir, file.getValue());
                    files.deleteFile(inputDir, file.getValue());
                } catch (IOException e) {
                    logger.logp(Level.SEVERE, Fiskal.class.getName(), "processFiles()",
                                "Can not copy input file to errors: "+e.getMessage());
                    throw new ProcessException(e);
                }
                continue;
            }   
            
            
            String oib = null;
            System.out.println("Setting header informations");    
            if (jaxbObject instanceof PoslovniProstorZahtjev) {
                PoslovniProstorZahtjev poslovniProstorZahtjev = (PoslovniProstorZahtjev)jaxbObject;
                setHeaderValues(poslovniProstorZahtjev.getZaglavlje());
                oib = poslovniProstorZahtjev.getPoslovniProstor().getOib();
            } 
            else if (jaxbObject instanceof RacunZahtjev) {
                RacunZahtjev racunZahtjev = (RacunZahtjev)jaxbObject;
                setHeaderValues(racunZahtjev.getZaglavlje());
                oib = racunZahtjev.getRacun().getOib();
                
                //Upisi zastitni kod
                try {
                    signer.zastitniKodRacuna(racunZahtjev,
                                            KeyConfig.getKey(oib),
                                            KeyConfig.getKeyDbPassword(oib), 
                                            KeyConfig.getKeyPassword(oib),
                                            KeyConfig.PKCS12);
                    System.out.println("Zastitini kod created");
                } catch (IOException ie) {
                    throw new ProcessException(ie);
                } catch (SignerException se) {
                    throw new ProcessException(se);
                }
            }else
                throw new ProcessException("Wrong jaxbObject passed as argument");
            
            
            
            
            
            //Signing
            StreamSource xmlSource = null;
            try {
                xmlSource = signer.getSignedXmlAsStream(ser, 
                                                        jaxbObject, 
                                                        refId, 
                                                        KeyConfig.getKey(oib), 
                                                        KeyConfig.getKeyDbPassword(oib),
                                                        KeyConfig.getKeyPassword(oib), 
                                                        KeyConfig.PKCS12);
                System.out.println("XML signed");
                
            } catch (IOException ie) {
                    throw new ProcessException(ie);
            } catch (SignerException se) {
                    throw new ProcessException(se);
            }
                
            //Write to out
            writeFile(ser, outDir, file, jaxbObject);
            System.out.println("File " + file.getValue() + " copied to out.");
            
            //Delete file if it is required by Config flag
            if (Config.IN_DELETE)
                try {
                    files.deleteFile(inputDir, file.getValue());
                } catch (IOException e) {
                    throw new ProcessException(e);
                }    
            
                
            try {
                //Call web service
                callService(respSer, xmlSource, resDir, file);
                System.out.println("WebService call executed successfully!");
                System.out.println(" ");

                //If we have reached this point delete succesfully processed file if it is not already deleted
                if (!Config.IN_DELETE)
                    try {
                        files.deleteFile(inputDir, file.getValue());
                    } catch (IOException e) {
                        throw new ProcessException(e);
                    }
            } catch (ProcessException pe) {
                //pe.printStackTrace();
                 //if (logger != null) {
                  //logger.logp(Level.SEVERE, Fiskal.class.getName(), "runAsDaemon()",
                  //          "Can not execute request: "+pe.getMessage());
                    logger.warning("Can not execute WebService request");
                    System.out.println(" ");
                //} else
                  //  System.err.println("Can not execute request: "+pe.getMessage());
            
            }
        }//for
        
        
    }//processFiles
    
    /**
     * Reads JSON or XML files from input dir and converts them to JAXB object
     * @param <T> Type of JAXB object that is returned from file
     * @param ser Serializer for reading input files
     * @param inputDir Derectory containing input files
     * @param file Key,Value pair with file type and file name
     * @param className JSON processing requires class name for transformer so
     *        we have to pass 'ProstoriZahtjev' or 'RacuniZahtjev'
     * @return JAXB object returned from file
     * @throws ProcessException
     */
    private <T> T readFile(Serializer<T> ser, 
                           String inputDir,
                           Map.Entry<Boolean, String> file,
                           String className) throws ProcessException {
        //Read from in
        try {
            if (file.getKey().equals(Boolean.TRUE))
                return ser.readJSON(inputDir + file.getValue(),
                                          Config.JAXB_PACKAGE +
                                          "."+ className);
            else
                return ser.readXML(inputDir + file.getValue());
            
            
        } catch (ConversionException ce) {
            ce.printStackTrace();
            logger.warning("Cannot process file: "+ file.getValue().toUpperCase()+" -bad content");
            throw new ProcessException(ce);
        }
    }//readFile
    
    /**
     * Serllizes JAXB object and writes JSON or XML file to otput directory
     * @param <T> Type o JAXB bovect to serialize
     * @param ser Serializer for JAX object
     * @param outputDir Otput directory to which to write the file
     * @param file Key,Value pair with type nad name of the file
     * @param jaxbObject JAXB object to serialize
     * @throws ProcessException
     */
    private <T> void writeFile(Serializer<T> ser, 
                               String outputDir,
                               Map.Entry<Boolean, String> file,
                               T jaxbObject) throws ProcessException {
        try {
            if (file.getKey().equals(Boolean.TRUE))
                ser.writeJSON(outputDir+file.getValue() ,jaxbObject);
            else
                ser.writeXML(outputDir+file.getValue() ,jaxbObject);
            
            
        } catch (ConversionException ce) {
            throw new ProcessException(ce);
        }
                           
    }//writeFile
    
    /**
     * Sets correct header values for JAXB objects
     * @param zaglavlje Header part of JAXB object
     */
    private void setHeaderValues(ZaglavljeType zaglavlje) throws ProcessException {        
        //Definiranje UUID-a
        zaglavlje.setIdPoruke(UUID.randomUUID().toString());

        //Definiraj datum
        try {
            SimpleDateFormat dateFormatter =
                new SimpleDateFormat("dd.MM.yyyy'T'HH:mm:ss");
            dateFormatter.setLenient(false);
            zaglavlje.setDatumVrijeme(dateFormatter.format(new Date()));
        } catch (IllegalArgumentException e) {
            throw new ProcessException(e);
        }
    }
    
    /**
     * Calls web service by passing XML as the payload
     * @param respSer Serializer for response object
     * @param xmlSource raw XML to send as the SOAP payload
     * @param responseDir output dir for response files
     * @param file Key,Value pair with type nad name of the file
     */
    private <T> void callService(Serializer<T> respSer, 
                                 StreamSource xmlSource, 
                                 String responseDir,
                                 Map.Entry<Boolean, String> file) throws ProcessException {
        
        
        
        try {
            //Call web service
            Source rawXML = fiskalizacijaClient.getRawXmlResponse(xmlSource);
            
            T jaxbResponse = respSer.getJaxbFromRawXML(rawXML);
            
            //Write response
            writeFile(respSer, responseDir, file, jaxbResponse);
 
        } catch (ConversionException e) {
            throw new ProcessException(e);
            
        }
        
    }//callService
    
}
