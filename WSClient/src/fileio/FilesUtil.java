package fileio;

import java.io.File;

import java.io.IOException;

import java.util.Map;
import java.util.TreeMap;
import se.datadosen.util.*;

public class FilesUtil {
    public FilesUtil() {
        super();
    }

    //private ArrayList<String> xmlList = new ArrayList<String>();
    //private ArrayList<String> jsonList = new ArrayList<String>();
    private Map<Boolean,String> fileLst = new TreeMap<Boolean,String>();


    /**
     * Fill list of files form the passed dir
     * @param inputDir Input directory from wich to form the list
     * @return list of files from directory
     */
    private void fillFileList(String inputDir) throws IOException {
        File inDir = new File(inputDir);
        File[] inFiles = inDir.listFiles();
        fileLst.clear();

        for (File file : inFiles) {
            String fileName = file.getName();
            if (fileName.substring(fileName.lastIndexOf(".")).equalsIgnoreCase(".xml"))
                fileLst.put(Boolean.FALSE, file.getName());
            else if (fileName.substring(fileName.lastIndexOf(".")).equalsIgnoreCase(".json"))
                fileLst.put(Boolean.TRUE, file.getName());
            else
                throw new IOException("The file is not XML or JSON file");
        }

    } //~fillFileList()


    public Map<Boolean, String> getFileLst(String inputDir) throws IOException {
        fillFileList(inputDir);
        return fileLst;
    }
    
    
    public void copyFile(String inDir, String inFile,
                String outDir, String outFile) throws IOException {
      String commandLine = null;
      
      File src = new File(inDir,inFile);
      File dest = new File(outDir, outFile);

        if (System.getProperty("os.name").toUpperCase().indexOf("WINDOWS") != -1 ) {
          //commandLine = "copy /y " + src.getAbsolutePath() + " " +
          //    dest.getAbsolutePath();
        
          IO fileIO = new IO();
          fileIO.copyFile(src, dest);
        } else {
          commandLine = "cp " + src.getAbsolutePath() + " " + dest.getAbsolutePath();
          Runtime runtime = Runtime.getRuntime();
          Process copyProc = runtime.exec(commandLine);
            try {
                copyProc.waitFor();
            } catch (InterruptedException e) {
                throw new IOException(e);
            }
            int retValue = copyProc.exitValue();
          if (retValue != 0)
              throw new IOException("Canot copy file "+ inFile);
        }//~if
          
    }//~copy()
    
    public void deleteFile(String dir, String file) throws IOException {
        File delFile = new File(dir, file);
        if (!delFile.delete())
            throw new IOException("Canot delete file " + file);
            
    }
}
