package fileio;

import java.io.FileInputStream;

import java.io.IOException;

import java.util.Properties;

public class LoadConfig {
    //private static LoadConfig singelton;
    Properties prop = new Properties();


    /* public static LoadConfig createIntance() throws IOException {
        if (singelton == null)
            singelton = new LoadConfig();
        return singelton;
    } */

    public LoadConfig(String configurationName) throws IOException {
        //load a properties file
        prop.load(new FileInputStream("resources/"+configurationName+".properties"));
    }


    public String getProperty(String key) {
        return prop.getProperty(key);   
    }
}
