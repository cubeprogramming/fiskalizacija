package ser;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;

import java.io.IOException;

import java.io.PrintWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * Converts Object to JSON
 */
public class JAXBExampleToJSON {
	public static void main(String[] args) {
 
	  Customer customer = new Customer();
	  customer.setId(100);
	  customer.setName("mkyong");
	  customer.setAge(29);
 
	  try {
 
		PrintWriter file = new PrintWriter(new FileWriter("file.json"));
                 
		//JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);
                Gson gson = new Gson();
                String json = gson.toJson(customer);
 
		// output pretty printed
		file.write(json);
              
                file.close();
 
	      } catch (IOException e) {
		e.printStackTrace();
	      }
 
	}
}
