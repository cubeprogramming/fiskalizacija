package ser;

import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class XMLSignerTest {
    public XMLSignerTest() {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * @see XMLSigner#signXmlDocumentOnDisk(String,String,String,String,String)
     */
    @Test
    public void testSignXmlDocumentOnDisk() {
        XMLSigner xMLSigner = new XMLSigner();
            
            
        try {
            xMLSigner.signXmlDocumentOnDisk("resources/poslovniProstori_in/poslovniProstorZahtjev.xml",
                                            "resources/poslovniProstori_out/poslovniProstorZahtjev_signed.xml", "resources/fiskal1.pfx",
                                            "614085",
                                            "poslovniProstorId");
            xMLSigner.signXmlDocumentOnDisk("resources/racuni_in/racunZahtjev.xml",
                                            "resources/racuni_out/racunZahtjev_signed.xml", "resources/fiskal1.pfx",
                                            "614085",
                                            "racunId");
        } catch (Exception e) {
            e.printStackTrace();
            throw new AssertionError(e.getMessage());
        }    
    }
}
