package ser;


import java.io.FileNotFoundException;
import java.io.IOException;


import java.security.cert.CertificateException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import java.util.*;

//import com.itextpdf.text.pdf.*;

import java.awt.Rectangle;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.tsp.TimeStampToken;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;


public class Potpisivanje {

    public static void main(String[] args) {

        try {
            KeyStore ks = KeyStore.getInstance("pkcs12");
            ks.load(new FileInputStream("my_private_key.pfx"),
                    "my_password".toCharArray());
            String alias = (String)ks.aliases().nextElement();
            PrivateKey key =
                (PrivateKey)ks.getKey(alias, "my_password".toCharArray());
            Certificate[] chain = ks.getCertificateChain(alias);
            /* PdfReader reader = new PdfReader("original.pdf");
            FileOutputStream fout = new FileOutputStream("signed.pdf");
            PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0');
            PdfSignatureAppearance sap = stp.getSignatureAppearance();
            sap.setCrypto(key, chain, null,
                          PdfSignatureAppearance.WINCER_SIGNED);
            sap.setReason("I'm the author");
            sap.setLocation("Lisbon");
            // comment next line to have an invisible signature
            sap.setVisibleSignature(new Rectangle(100, 100, 200, 200), 1,
                                    null);
            stp.close(); */
        } catch (KeyStoreException kse) {
            // TODO: Add catch code
            kse.printStackTrace();
        } catch (UnrecoverableKeyException uke) {
            // TODO: Add catch code
            uke.printStackTrace();
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            // TODO: Add catch code
            ioe.printStackTrace();
        } catch (CertificateException ce) {
            // TODO: Add catch code
            ce.printStackTrace();
        } catch (NoSuchAlgorithmException nsae) {
            // TODO: Add catch code
            nsae.printStackTrace();
        }
    }

}
