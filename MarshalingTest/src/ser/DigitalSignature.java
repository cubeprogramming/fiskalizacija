package ser;



import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.ObjectInput;

import java.io.ObjectInputStream;

import java.io.StringWriter;

import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;

import java.util.Collections;

import javax.xml.crypto.*;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.keyinfo.*;
import javax.xml.crypto.dsig.spec.*;
import javax.xml.crypto.dom.*;
import javax.xml.crypto.dsig.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DigitalSignature {
    
    public static void main(String[] args) {
        
             PrivateKey pk = null;
                javax.security.cert.X509Certificate cert = null;
                try{
                    /* ByteArrayInputStream bis = new ByteArrayInputStream(keyCer.getLlave());
                    ObjectInput in = new ObjectInputStream(bis);
                    pk = (PrivateKey) in.readObject();
                    bis.close(); */
                    
                       FileInputStream file_inputstream = new FileInputStream("D:\\JAVA-projekti\\fina.jks");
                       KeyStore keyStore = KeyStore.getInstance( "JKS" );
                       keyStore.load( file_inputstream, "614085".toCharArray() );
                       pk = (PrivateKey)keyStore.getKey( "fina", "614085".toCharArray() );

                    /* bis = new ByteArrayInputStream(keyCer.getCertificado());
                    in = new ObjectInputStream(bis);
                    cert = (javax.security.cert.X509Certificate) in.readObject();
                    bis.close(); */

                    //keyCer.setCertB64(Base64.encodeBase64String(cert.getEncoded()));

                    DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
                    dbfac.setNamespaceAware(true);
                    DocumentBuilder docBuilder;
                    docBuilder = dbfac.newDocumentBuilder();
                    DOMImplementation domImpl = docBuilder.getDOMImplementation();
                    Document doc = domImpl.createDocument("http://cancelacfd.sat.gob.mx", "Cancelacion", null);
                    doc.setXmlVersion("1.0");
                    doc.setXmlStandalone(true);

                    Element cancelacion = doc.getDocumentElement();
                    cancelacion.setAttributeNS("http://www.w3.org/2000/xmlns/","xmlns:xsd","http://www.w3.org/2001/XMLSchema");
                    cancelacion.setAttributeNS("http://www.w3.org/2000/xmlns/","xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
                    //cancelacion.setAttribute("RfcEmisor", rfc);
                    //cancelacion.setAttribute("Fecha", fecha);

                    Element folios = doc.createElement("Folios");
                    cancelacion.appendChild(folios);
                    /* for (int i=0; i<uuid.length; i++) {
                        Element u = doc.createElement("UUID");
                        u.setTextContent(uuid[i]);
                        folios.appendChild(u);
                    } */

                    DOMSignContext dsc = new DOMSignContext (pk, doc.getDocumentElement());
                    XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

                    Reference ref = fac.newReference ("", fac.newDigestMethod(DigestMethod.SHA1, null),
                                Collections.singletonList
                                (fac.newTransform(Transform.ENVELOPED,
                                (TransformParameterSpec) null)), null, null);

                    SignedInfo si = fac.newSignedInfo
                              (fac.newCanonicalizationMethod
                                (CanonicalizationMethod.INCLUSIVE_WITH_COMMENTS,
                                  (C14NMethodParameterSpec) null),
                                fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
                                Collections.singletonList(ref));

                    KeyInfoFactory kif = fac.getKeyInfoFactory();
                    KeyValue kv = kif.newKeyValue(cert.getPublicKey());
                    KeyInfo ki = kif.newKeyInfo(Collections.singletonList(kv));

                    XMLSignature signature = fac.newXMLSignature(si, ki);

                    signature.sign(dsc);

                    TransformerFactory transfac = TransformerFactory.newInstance();
                    Transformer trans = transfac.newTransformer();
                    trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                    trans.setOutputProperty(OutputKeys.VERSION, "1.0");
                    trans.setOutputProperty(OutputKeys.INDENT, "yes");

                    //CREAR STRING DEL ARBOL XML
                    StringWriter sw = new StringWriter();
                    StreamResult result = new StreamResult(sw);
                    DOMSource source = new DOMSource(doc);
                    trans.transform(source, result);
                    String xmlString = sw.toString();
                    System.out.println(xmlString);

                   } catch (Exception e) {
                       e.printStackTrace();
                   }
    }
    
}
