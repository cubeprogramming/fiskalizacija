/*
* @(#)ZastitniKodIzracun.java
*
* Project: Fiskalizacija
*
* Copyright (c) APIS IT d.o.o. Paljetkova 18 Zagreb, Hrvatska
*/
// Class-Path: commons-codec-1.7.jar commons-codec-1.7-test-sources.jar

// package primjer;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import org.apache.commons.codec.digest.DigestUtils;
/**
* ZastitniKodIzracun - klasa za izra�un za�titnog broja napisana tako da prati
pseudokod.
*/
public class ZastitniKodIzracun {
	public static void main( String[] args ) {
		// po�etak
		// pro�itaj (oib)
		String oib = "00169331406";
		// medjurezultat = oib
		String medjurezultat = oib;
		// pro�itaj (datVrij � datum i vrijeme izdavanja ra�una zapisain kao tekst u formatu 'dd.MM.gggg HH:mm:ss')
		String datVrij = new SimpleDateFormat( "dd.MM.yyyy HH:mm:ss" ).format( new Date() );
		// medjurezultat = medjurezultat + datVrij
		medjurezultat = medjurezultat + datVrij;
		// pro�itaj (bor � broj�ana oznaka ra�una)
		String bor = "12345";
		// Stranica 50 od 52
		// medjurezultat = medjurezultat + bor
		medjurezultat = medjurezultat + bor;
		// pro�itaj (opp � oznaka poslovnog prostora)
		String opp = "blag001";
		// medjurezultat = medjurezultat + opp
		medjurezultat = medjurezultat + opp;
		// pro�itaj (onu � oznaka naplatnog ure�aja)
		String onu = "11245";
		// medjurezultat = medjurezultat + onu
		medjurezultat = medjurezultat + onu;
		// pro�itaj ( uir - ukupni iznos ra�una )
		String uir = "1245.56";
		// medjurezultat = medjurezultat + uir
		medjurezultat = medjurezultat + uir;
		// elektroni�ki potpi�i medjurezultat koriste�i RSA-SHA1 potpis
		byte[] potpisano = null;
		try {
			FileInputStream file_inputstream = new FileInputStream("D:\\JAVA-projekti\\fina.jks");
			KeyStore keyStore = KeyStore.getInstance( "JKS" );
			keyStore.load( file_inputstream, "614085".toCharArray() );
			Key privatni = keyStore.getKey( "fina", "614085".toCharArray() );
			Signature biljeznik = Signature.getInstance( "SHA1withRSA" );
			biljeznik.initSign( ( PrivateKey )privatni );
			biljeznik.update( medjurezultat.getBytes() );
			potpisano = biljeznik.sign();
		}
		catch ( Exception e ) {
			// nije uspjelo �itanje privatnog klju�a
			e.printStackTrace();
		}
		// rezultatIspis = izra�unajMD5(elektroni�ki potpisani medjurezultat)
		String rezultatIspis = DigestUtils.md5Hex( potpisano );
		// kraj
		System.out.println( "Dobiveni 32-znamenkasti za�titni kod je: " + rezultatIspis);
	}
}